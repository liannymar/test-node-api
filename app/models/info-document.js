const mongoose = require('mongoose');
const mongooseUniqueValidator = require('mongoose-unique-validator');

const infoDocument = new mongoose.Schema({
  id: { type: String, unique: true, required: true },
  name: { type: String, required: true },
  lastName: { type: String, required: true },
  email: { type: String, required: false },
  date: { type: Date, required: true },
});

infoDocument.plugin(mongooseUniqueValidator);
module.exports = mongoose.model('info', infoDocument);
