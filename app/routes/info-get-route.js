const express = require('express');
const helper = require('../common/commonHelper');
const logger = require('../loggers/logger');
const InfoController = require('../controllers/info-controller');

const router = express.Router();

function handler(request, response) {
  let getInfoDTO;
  let toReturn;
  try {
    getInfoDTO = request.params.id;
  } catch (err) {
    logger.debug('access not allowed, body: %s', JSON.stringify(request.params.id));
    helper.setResponseWithError(response, 403, 'Invalid Parameter');
  }
  const infoController = new InfoController();
  infoController.getInfo(getInfoDTO)
  .then((result) => {
    toReturn = helper.setResponse(response, result);
  });
  return toReturn;
}

router.get('/info/:id', handler);

module.exports = router;
module.exports.handler = handler;
