const express = require('express');
const helper = require('../common/commonHelper');
const logger = require('../loggers/logger');
const SaveInfoDTO = require('../dtos/save-info-dto');
const SaveInfoController = require('../controllers/save-info-controller');

const router = express.Router();

function handler(request, response) {
  let saveInfoDTO;
  let toReturn;
  try {
    saveInfoDTO = new SaveInfoDTO(request.body);
  } catch (err) {
    // console.log(request.body);
    // console.log(err);
    logger.debug('access not allowed, body: %s', JSON.stringify(request.body));
    helper.setResponseWithError(response, 403, 'Invalid parameters');
  }
  const saveInfoController = new SaveInfoController();
  saveInfoController.saveInfo(saveInfoDTO)
  .then((result) => {
    toReturn = helper.setResponse(response, result);
  }).catch((error) => {
    toReturn = helper.setResponse(response, error);
  });
  return toReturn;
}

router.post('/save-info', handler);

module.exports = router;
module.exports.handler = handler;
