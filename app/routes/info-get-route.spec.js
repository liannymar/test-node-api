var mockery = require('mockery');
var httpMocks = require('node-mocks-http');
var EventEmitter = require("events").EventEmitter;

describe('infoGet route', () => {
  beforeEach(() => {
    mockery.enable({
      warnOnReplace: false,
      warnOnUnregistered: false,
      useCleanCache: true
    });
    mockery.registerMock('../../loggers/logger', {
      debug: function () {}, 
      error: function() {}, 
      info: function() {}
    });
  });

  afterEach(() => {
    mockery.disable();
    mockery.deregisterAll();
  });

  describe('Test Handler Function', () => {
    it('should not have errors', () => {
      // mock del controller
      class InfoControllerMock {
        getInfo(getInfoDTO) {
          return new Promise((resolve, reject) => {
            resolve({field1:"ok", field2: "ok"});
          });
        }
      }

      mockery.registerMock("../controllers/info-controller", InfoControllerMock);

      // mock del request 
      let request = httpMocks.createRequest({
        url: "/", 
        headers: {"x-access-token": "M2UzOTUxZjNkZWYwNGQzMzJiYWJlYTE4d"},
        body: {
          "id": 1,
          "name": "Lala",
          "lastName": "Lolo",
          "email": "mail@gmail.com",
          "test": "11.111.111-1"
        }
      });

      // mock del response
      let response = httpMocks.createResponse({
        eventEmitter: EventEmitter
      });

      // invocar a la ruta
      route = require("./info-get-route");
      route.handler(request, response);

      expect(response.statusCode).toBe(200);
    });

    it('should return error when the body is incorrect', () => {
      // mock del controller
      class InfoControllerMock {
    	getInfo(getInfoDTO) {
          return new Promise((resolve, reject) => {
            resolve({field1:"ok", field2: "ok"});
          });
        }
      }

      mockery.registerMock("../controllers/info-controller", InfoControllerMock);

      // mock del request 
      let request = httpMocks.createRequest({
        url: "/", 
        headers: {"x-access-token": "M2UzOTUxZjNkZWYwNGQzMzJiYWJlYTE4d"},
        body: {}
      });

      // mock del response
      let response = httpMocks.createResponse({
        eventEmitter: EventEmitter
      });

      // invocar a la ruta
      route = require("./info-get-route");
      route.handler(request, response);

      expect(response.statusCode).toBe(200);
     //expect(response._getData().message).toBe("Invalid parameters");
  });

  it("should return an error when something is wrong with mongo", () => {
      // mock de la clase new InfoDTO(request.body);
      
      class GetInfoDTOMock {
        constructor(body) {
          throw "Body error";
        }
      }

      mockery.registerMock('../controller/info-controller', GetInfoDTOMock);
      
      // mock del controller
      class InfoControllerMock {
    	getInfo(getInfoDTO) {
          return new Promise((resolve, reject) => {
            resolve({field1:"ok", field2: "ok"});
          });
        }
      }

      mockery.registerMock("../controllers/info-controller", InfoControllerMock);

      // mock del request 
      let request = httpMocks.createRequest({
        url: "/", 
        headers: {"x-access-token": "M2UzOTUxZjNkZWYwNGQzMzJiYWJlYTE4d"},
        body: {
          "id": 1,
          "name": "Lala",
          "lastName": "Lolo",
          "email": "mail@gmail.com",
          "test": "11.111.111-1"
        }
      });

      // mock del response
      let response = httpMocks.createResponse({
        eventEmitter: EventEmitter
      });

      // invocar a la ruta
      route = require("./info-get-route");
      route.handler(request, response);

      expect(response.statusCode).toBe(200);
    });

    it("should return an error when the controller fails", () => {
      // mock del controller
      class InfoControllerMock {
    	 getInfo(getInfoDTO) {
          return new Promise((resolve, reject) => {
            reject({});
          });
        }
      }

      mockery.registerMock("../controllers/info-controller", InfoControllerMock);

      // mock del request 
      let request = httpMocks.createRequest({
        url: "/", 
        headers: {"x-access-token": "M2UzOTUxZjNkZWYwNGQzMzJiYWJlYTE4d"},
        body: {
          "id": 1,
          "name": "Lala",
          "lastName": "Lolo",
          "email": "mail@gmail.com",
          "test": "11.111.111-1"
        }
      });

      // mock del response
      let response = httpMocks.createResponse({
        eventEmitter: EventEmitter
      });

      // invocar a la ruta
      route = require("./info-get-route");
      route.handler(request, response);

      expect(response.statusCode).toBe(200);
      //expect(response._getData().message).toBe("message");
    });
  });
});