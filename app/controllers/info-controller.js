const InfoMongoService = require('../services/persistence/info-mongo-service');

module.exports = class getInfoController {
  getInfo(id) {
    const infoMongoService = new InfoMongoService();
    return new Promise((resolve, reject) => {
      infoMongoService.getInfo(id)
      .then((doc) => {
        resolve(doc);
      })
      .catch((error) => {
        reject(error);
      });
    });
  }
};
