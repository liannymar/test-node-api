var mockery = require('mockery');
var sinon = require('sinon');

describe("getInfoController module", () => {
  let GetInfoController = null;
  let getInfoController = null;
  let originalTimeout;

  beforeEach(() => {
    mockery.enable({
      warnOnReplace: false,
      warnOnUnregistered: false,
      useCleanCache: true
    });

    mockery.registerMock('../loggers/logger.js', {
      info: (param, param2) => {},
      debug: (param) => {}}
    );

    mockery.registerMock('../../config/index', {});
    originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;          
  });

  afterEach(() => {
    GetInfoController = null;
    getInfoController = null;
    mockery.disable();
    mockery.deregisterAll();
    jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
  });

  it("Should be an instance of getInfoController", () => {                                  
    GetInfoController = require('./info-controller');
    getInfoController = new GetInfoController();                  
    expect(getInfoController instanceof GetInfoController).toBeTruthy();     
  });

  describe("getInfo method", () => {
    it("should return a promise ", (done) => {
      // mock del servicio de mongo
      let checkObject = {used: () => {}}; 

      class InfoMongoServiceMock {
        getInfo(info) {
          checkObject.used();
          return new Promise((resolve, reject) => {
            resolve({status:"ok", response:"ok"});
            //reject("error");
          });
        }
      }
      
      let checkObjectSpy = sinon.spy(checkObject, 'used'); 
      mockery.registerMock('../services/persistence/info-mongo-service', InfoMongoServiceMock);
      
      // instanciar el sujeto de pruebas
      GetInfoController = require('./info-controller');
      getInfoController = new GetInfoController();               
      let Info = require('../models/info-document');
      let info =  new Info({id:"1", name:"Nombre", lastName:"Apellido", email:"napellido@correo.com", date:""});
      let id = info.id;
      
      // probar el sujeto para que de OK
      getInfoController.getInfo(id).then((response) => {
        expect(response.status).toBe("ok");
        expect(response.response).toBe("ok");
        expect(checkObjectSpy.callCount).toBe(1);
        done();
      })
      .catch((error) => {
        throw "should be in the then block";
      });
    });

    it("should return an error ", (done) => {
      // mock del servicio de mongo
      let checkObject = {used: () => {}}; 

      class InfoMongoServiceMock {
        getInfo(id) {
          checkObject.used();
          return new Promise((resolve, reject) => {
            reject({status:"error", response:"error"});
            // resolve({status:"ok", response:"ok"});
          });
        }
      }
      let checkObjectSpy = sinon.spy(checkObject, 'used'); 
      mockery.registerMock('../services/persistence/info-mongo-service', InfoMongoServiceMock);
      
      // instanciar el sujeto de pruebas
      GetInfoController = require('./info-controller');
      getInfoController = new GetInfoController();
      let Info = require('../models/info-document');
      let info =  new Info({id:"1", name:"Nombre", lastName:"Apellido", email:"napellido@correo.com", date:""});
      let id = info.id;
      
      // probar el sujeto para que de error
      getInfoController.getInfo(id).then((response) => {
        throw "should be in the catch block";
      })
      .catch((error) => {
        expect(error.status).toBe("error");
        expect(error.response).toBe("error");
        expect(checkObjectSpy.callCount).toBe(1);
        done();
      });
    });
  });

});
