const InfoMongoService = require('../services/persistence/info-mongo-service');

module.exports = class SaveInfoController {
  saveInfo(saveInfoDTO) {
    const infoMongoService = new InfoMongoService();
    return new Promise((resolve, reject) => {
      const document = this.setDocument(saveInfoDTO);
      infoMongoService.saveInfo(document)
      .then((doc) => {
        resolve(doc);
      })
      .catch((error) => {
        reject(error);
      });
    });
  }

  setDocument(dto) {
    const doc = {};
    doc.id = dto.id;
    doc.name = dto.name;
    doc.lastName = dto.lastName;
    doc.email = dto.email;
    return doc;
  }

};
